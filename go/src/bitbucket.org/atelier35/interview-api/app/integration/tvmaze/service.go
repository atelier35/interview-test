package tvmaze

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"bitbucket.org/atelier35/interview-api/app/external"
	"fmt"
)

type Service struct {
	client Client
}

func NewService() Service {
	return Service{
		client: NewClient(),
	}
}

func (s Service) SearchTVShows(keywords string) (shows []external.TVShow, err errors.DetailedError) {
	res, err := s.client.GetCollection(fmt.Sprintf("/search/shows?q=%s", keywords))
	if err != nil {
		return shows, err
	}
	for _, show := range res {
		shows = append(shows, NewTVShow(show))
	}
	return shows, nil
}
