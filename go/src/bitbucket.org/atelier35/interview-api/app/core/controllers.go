package core

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"fmt"
	"github.com/codegangsta/negroni"
	go_errors "github.com/go-errors/errors"
	"github.com/sarulabs/di"
	"log"
	"net/http"
	"os"
)

func WriteAllowOriginHeader(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
}

func OptionHandler(w http.ResponseWriter, r *http.Request, next http.HandlerFunc, ctn di.Container) {
	WriteAllowOriginHeader(w, r)
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Max-Age", "1000")
	w.Header().Set("Access-Control-Allow-Headers", "origin, x-csrftoken, content-type, accept, authorization")
	w.WriteHeader(http.StatusNoContent)
}

func logErrorFromRequest(request ApiRequest, error error) {
	extra := make(map[string]string)
	LogError(error, extra)
}

func LogError(error error, extra map[string]string) {
	eventID := "local-error"
	message := fmt.Sprintf("Captured error with id %s: %s", eventID, error.Error())
	log.Println(message)
}

func NewActionControllerHandler(handler ApiControllerHandler, ctn di.Container) negroni.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		var err errors.DetailedError
		var response ApiResponse

		request, err := NewApiRequest(r)
		defer func() {
			rec := recover()
			if rec != nil {
				switch t := rec.(type) {
				case string:
					err = errors.NewSimpleError(t)
					logErrorFromRequest(request, err)
				case error:
					message := fmt.Sprintf(
						"Fatal error : %s\nTrace : %s",
						t.Error(),
						go_errors.Wrap(t.Error(), 2).ErrorStack(),
					)
					err = errors.NewSimpleError(message)
				default:
					err = errors.NewSimpleError("Unknown error")
					logErrorFromRequest(request, err)
				}
				response = NewErrorApiResponse(err, http.StatusInternalServerError)
				response.Write(w, r)
				next(w, r)
				fmt.Println("[ERROR]", err.Error())
				if GetEnvironment() == EnvironmentDevelopment {
					os.Exit(1)
				}
			}
		}()

		if err != nil {
			response = NewErrorApiResponse(err, http.StatusBadRequest)
			response.Write(w, r)
			next(w, r)
			return
		}

		reqContainer, _ := ctn.SubContainer()
		defer reqContainer.Delete()
		response = handler(request, reqContainer)
		if response.Status >= 500 {
			if anError, ok := response.Body.(errors.DetailedError); ok {
				logErrorFromRequest(request, anError)
			}
		}
		response.Write(w, r)
		next(w, r)
	}
}
