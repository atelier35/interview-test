package core

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"go/build"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"time"
)

const (
	EnvironmentDevelopment = "dev"
	EnvironmentStaging     = "staging"
	EnvironmentProduction  = "prod"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func GetEnvironment() string {
	return os.Getenv("APP_ENV")
}

func BasePath(path string) string {
	filePrefix := "src/bitbucket.org/atelier35/interview-api/"
	return filepath.Join(build.Default.GOPATH, filePrefix, path)
}

// It returns the number of bytes written and any write error encountered.
// ExtractLanguageTagAndCurrencyFromHttpHeader returns the best default language and currency from an "acceptLanguage" HTTP header
// It returns the language tag as per ietf BCP47 standard https://godoc.org/golang.org/x/text/language and the Currency as per ISO4217 3 letter currency code https://godoc.org/golang.org/x/text/currency
func ExtractLanguageTagAndCurrencyFromHttpHeader(acceptLanguageHeader string) (string, string) {
	var supported = []language.Tag{ //the first language in the list is the fallback
		language.AmericanEnglish, // en-US
		language.CanadianFrench,  //fr-CA
	}
	var matcher = language.NewMatcher(supported)

	tag, _ := language.MatchStrings(matcher, acceptLanguageHeader)
	currency, _ := currency.FromTag(tag)

	return tag.String(), currency.String()
}

func GetStructTagsFromObject(object interface{}, tagName string) []string {
	val := reflect.ValueOf(object)
	var tagValues []string
	for i := 0; i < val.Type().NumField(); i++ {
		tagValues = append(tagValues, val.Type().Field(i).Tag.Get(tagName))
	}
	return tagValues
}

func RemoveFromSlice(l []string, item string) []string {
	for i, other := range l {
		if other == item {
			return append(l[:i], l[i+1:]...)
		}
	}
	return l
}

func GetKeysFromMap(data map[string]interface{}) []string {
	keys := make([]string, 0, len(data))
	for k := range data {
		keys = append(keys, k)
	}
	return keys
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func RemoveFromStringSlice(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func UniqueStringSlice(slice []string) (items []string) {
	u := make([]string, 0, len(slice))
	m := make(map[string]bool)
	for _, val := range slice {
		if _, ok := m[val]; !ok {
			m[val] = true
			u = append(u, val)
		}
	}
	return u
}

func ConvertInterfaceToStringSlice(slice []interface{}) []string {
	var stringSlice []string
	for _, v := range slice {
		if val, ok := v.(string); ok && val != "" {
			stringSlice = append(stringSlice, val)
		}
	}
	return stringSlice
}

func ConvertInterfaceToInterfaceSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		return nil
	}
	ret := make([]interface{}, s.Len())
	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}
	return ret
}

func ConvertInterfaceToMapString(m interface{}) map[string]string {
	mapString := make(map[string]string)
	v := reflect.ValueOf(m)
	if v.Kind() == reflect.Map {
		for _, key := range v.MapKeys() {
			value := v.MapIndex(key)
			mapString[key.Interface().(string)] = value.Interface().(string)
		}
	}
	return mapString
}

func ConvertInterfaceToMapInterface(m interface{}) map[string]interface{} {
	mapString := make(map[string]interface{})
	v := reflect.ValueOf(m)
	if v.Kind() == reflect.Map {
		for _, key := range v.MapKeys() {
			value := v.MapIndex(key)
			mapString[key.Interface().(string)] = value.Interface()
		}
	}
	return mapString
}

func ConvertInterfaceToMapBool(m interface{}) map[string]bool {
	mapBool := make(map[string]bool)
	v := reflect.ValueOf(m)
	if v.Kind() == reflect.Map {
		for _, key := range v.MapKeys() {
			value := v.MapIndex(key)
			mapBool[key.Interface().(string)] = value.Interface().(bool)
		}
	}
	return mapBool
}

func ConvertInterfaceToMapInterfaceSlice(slice []interface{}) []map[string]interface{} {
	var mapSlice []map[string]interface{}
	for _, v := range slice {
		mapSlice = append(mapSlice, ConvertInterfaceToMapInterface(v))
	}
	return mapSlice
}

func ConvertInterfaceToString(val interface{}) string {
	str, _ := val.(string)
	return str
}

func ConvertStringToFloat(val string) float64 {
	fl, _ := strconv.ParseFloat(val, 64)
	return fl
}

func MaxIntFromSlice(slice []int) (max int) {
	max = slice[0]
	for _, v := range slice {
		if v > max {
			max = v
		}
	}
	return max
}

func RandPassword(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func GetFileContentType(out *os.File) string {
	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)
	_, err := out.Read(buffer)
	if err != nil {
		return "application/octet-stream"
	}
	_, err = out.Seek(0, 0)
	if err != nil {
		return "application/octet-stream"
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	return http.DetectContentType(buffer)
}

func UniqueExternalID(integrationKey string, externalID string) string {
	return fmt.Sprintf("%s_%s", integrationKey, externalID)
}

func TokenizeMap(data map[string]interface{}) string {
	bytes, _ := json.Marshal(data)
	return base64.StdEncoding.EncodeToString(bytes)
}

func IsSameDay(a time.Time, b time.Time) bool {
	return a.Day() == b.Day() && a.Month() == b.Month() && a.Year() == b.Year()
}

func FormatDate(date *time.Time) string {
	if date == nil {
		return ""
	}
	return date.Format(time.RFC3339)
}

func FormatIndexedDate(date *time.Time) interface{} {
	if date == nil {
		return nil
	}
	return date.Format(time.RFC3339)
}

func FormatSlice(value interface{}) []interface{} {
	slice := ConvertInterfaceToInterfaceSlice(value)
	if len(slice) == 0 {
		return make([]interface{}, 0)
	}
	return slice
}

func IsNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}
