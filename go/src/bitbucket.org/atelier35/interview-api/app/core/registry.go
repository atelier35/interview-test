package core

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"github.com/sarulabs/di"
	"net/http"
)

type ApiControllerHandler func(request ApiRequest, ctn di.Container) ApiResponse
type PublicHandlerFunc func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc, ctn di.Container)

type ApiRouteDefinition struct {
	Method  string
	Path    string
	Handler ApiControllerHandler
}

type RouteDefinition struct {
	Method  string
	Path    string
	Handler PublicHandlerFunc
}

const (
	HEALTH_OK      = 1
	HEALTH_WARNING = 2
	HEALTH_ERROR   = 3
)

type Register interface {
	Name() string
	ServicesDefinition(container *di.Builder)
	Initialize(container di.Container) errors.DetailedError
	ApiRoutes() []ApiRouteDefinition
	Health(container di.Container) (int, map[string]string)
}
