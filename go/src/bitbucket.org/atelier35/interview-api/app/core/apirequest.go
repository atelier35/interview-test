package core

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"strings"
)

type ApiRequest struct {
	PathVars    map[string]string
	Body        map[string]interface{}
	Query       map[string][]string
	BodyRead    []byte
	httpRequest *http.Request
}

func NewApiRequest(r *http.Request) (ApiRequest, errors.DetailedError) {
	request := ApiRequest{
		Query: r.URL.Query(),
	}
	request.PathVars = mux.Vars(r)
	request.httpRequest = r
	if strings.Contains(r.Header.Get("Content-Type"), "application/json") {
		err := request.loadJsonBody(r)
		if err != nil {
			return request, err
		}
	}
	return request, nil
}

func (ar ApiRequest) GetUrlQuery() url.Values {
	return ar.httpRequest.URL.Query()
}

func (ar ApiRequest) GetHttpRequest() *http.Request {
	return ar.httpRequest
}

func (ar *ApiRequest) loadJsonBody(r *http.Request) errors.DetailedError {
	var err errors.DetailedError
	ar.BodyRead, err = GetJsonBodyFromRequest(r, &ar.Body)
	return err
}

func (ar ApiRequest) FormFile(field string) (multipart.File, *multipart.FileHeader, errors.DetailedError) {
	var file multipart.File
	var header *multipart.FileHeader
	var err error
	file, header, err = ar.httpRequest.FormFile(field)
	if err == http.ErrMissingFile {
		return file, header, errors.NewSimpleError("File is missing : %s", field)
	}
	if err != nil {
		return file, header, errors.NewSimpleError("Error getting file from the form : %s", err.Error())
	}
	return file, header, nil
}

//Returns the IP address of the client, even if proxied
func (ar ApiRequest) GetRemoteIpAddress() string {
	if ar.httpRequest.Header.Get("x-forwarded-for") != "" {
		return ar.httpRequest.Header.Get("x-forwarded-for")
	}
	return ar.httpRequest.RemoteAddr
}

func (ar ApiRequest) JsonDecode(object interface{}) errors.DetailedError {
	err := json.Unmarshal(ar.BodyRead, &object)
	if err != nil {
		return errors.NewSimpleError("Malformed json body: %s", err.Error())
	}
	return nil
}

func (ar ApiRequest) GetJsonBody() string {
	if ar.httpRequest.Method == http.MethodGet ||
		ar.httpRequest.Method == http.MethodDelete ||
		ar.httpRequest.Method == http.MethodOptions {
		return ""
	}
	return string(ar.BodyRead[:])
}

func GetJsonBodyFromRequest(r *http.Request, object interface{}) ([]byte, errors.DetailedError) {
	body, err := ioutil.ReadAll(r.Body)
	if len(body) == 0 {
		return body, nil
	}
	if err != nil {
		return body, errors.NewSimpleError("Malformed json body")
	}
	_ = json.Unmarshal(body, &object)
	return body, nil
}
