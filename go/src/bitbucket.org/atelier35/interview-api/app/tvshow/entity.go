package tvshow

import (
	"bitbucket.org/atelier35/interview-api/app/external"
)

type TVShow struct {
	Name      string `json:"name"`
	Language  string `json:"language"`
	Summary   string `json:"summary"`
	ImageLink string `json:"imageLink"`
	Url       string `json:"url"`
}

func NewTVShow(show external.TVShow) TVShow {
	return TVShow{
		Name:      show.GetName(),
		Language:  show.GetLanguage(),
		Summary:   show.GetSummary(),
		ImageLink: show.GetImageLink(),
		Url:       show.GetUrl(),
	}
}
