package tvshow

import (
	"bitbucket.org/atelier35/interview-api/app/core"
	"net/http"
)

type Controller struct {
	service ServiceInterface
}

func NewController(service ServiceInterface) Controller {
	return Controller{
		service: service,
	}
}

func (c Controller) Find(request core.ApiRequest) core.ApiResponse {
	keywords := ""
	q := request.Query["q"]
	if len(q) > 0 {
		keywords = q[0]
	}
	collection, err := c.service.Search(request.PathVars["integration"], keywords)
	if err != nil {
		return core.NewErrorApiResponse(err, http.StatusBadRequest)
	}
	return core.NewCollectionApiResponse(collection, http.StatusOK)
}
