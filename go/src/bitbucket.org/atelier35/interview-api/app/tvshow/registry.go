package tvshow

import (
	"bitbucket.org/atelier35/interview-api/app/core"
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"bitbucket.org/atelier35/interview-api/app/integration"
	"github.com/sarulabs/di"
)

type Registry struct{}

func (r Registry) ServicesDefinition(container *di.Builder) {
	_ = container.Add(di.Def{
		Name:  "tv-show-service",
		Scope: di.Request,
		Build: func(ctx di.Container) (interface{}, error) {
			return NewService(
				ctx.Get("integration-service").(integration.Service),
			), nil
		},
	})
	_ = container.Add(di.Def{
		Name:  "tv-show-controller",
		Scope: di.Request,
		Build: func(ctx di.Container) (interface{}, error) {
			return NewController(ctx.Get("tv-show-service").(ServiceInterface)), nil
		},
	})
}

func (r Registry) Name() string {
	return "tv-show"
}

func (r Registry) Initialize(container di.Container) errors.DetailedError { return nil }

func (r Registry) ApiRoutes() []core.ApiRouteDefinition {
	controller := func(ctn di.Container) Controller { return ctn.Get("tv-show-controller").(Controller) }
	return []core.ApiRouteDefinition{
		{Method: "GET", Path: "/tv-shows/{integration}/search", Handler: func(r core.ApiRequest, ctn di.Container) core.ApiResponse {
			return controller(ctn).Find(r)
		}},
	}
}

func (r Registry) Health(container di.Container) (int, map[string]string) {
	return core.HEALTH_OK, map[string]string{}
}
