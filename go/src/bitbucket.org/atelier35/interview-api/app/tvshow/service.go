package tvshow

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"bitbucket.org/atelier35/interview-api/app/integration"
)

type ServiceInterface interface {
	Search(integrationKey string, keywords string) (shows []TVShow, err errors.DetailedError)
}

type Service struct {
	integrationService integration.Service
}

func NewService(integrationService integration.Service) ServiceInterface {
	return &Service{
		integrationService: integrationService,
	}
}

func (s Service) Search(integrationKey string, keywords string) (shows []TVShow, err errors.DetailedError) {
	items, err := s.integrationService.SearchTVShows(integrationKey, keywords)
	if err != nil {
		return shows, err
	}
	for _, item := range items {
		shows = append(shows, NewTVShow(item))
	}
	return shows, nil
}
