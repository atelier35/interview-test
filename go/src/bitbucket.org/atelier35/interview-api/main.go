package main

import (
	"log"
	"os"
)

func main() {
	a := App{}
	err := a.Initialize()
	if err != nil {
		os.Exit(1)
		return
	}
	log.Fatal(a.RunHttp(":" + os.Getenv("HTTP_PORT")))

}
