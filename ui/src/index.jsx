import React from 'react'
import { render } from 'react-dom'
import App from './app.component.jsx'
import Main from './common/main.component.jsx'
import 'bootstrap'
import './common/app.scss'

render(
    <App><Main /></App>,
    document.getElementById('react-container')
);