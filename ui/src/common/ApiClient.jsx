'use strict'

class ApiClient
{
    constructor() {
        this.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        this.baseUrl = '/api'
    }

    transformer(res) {
        if (res.error) {
            this.handleError(res)
        }
        return Promise.resolve(res)
    }

    get(path, useLocalPath) {
        if (typeof useLocalPath === 'undefined') { useLocalPath = false; }

        var pathToFetch = this.baseUrl + path;
        if (useLocalPath)
            pathToFetch = path

        return fetch(pathToFetch, {
            method: "GET",
            headers: this.headers,
            credentials: "same-origin"
        })
            .then(res => res.json())
            .then(res => this.transformer(res))
            .catch(reason => {
                return Promise.resolve(reason)
            })
    }

    post(path, data) {
        return fetch(this.baseUrl + path, {
            method: "POST",
            body: JSON.stringify(data),
            headers: this.headers,
            credentials: "same-origin"
        })
            .then(res => res.json())
            .then(res => this.transformer(res))
            .catch(reason => {
                return Promise.resolve(reason)
            })
    }

    patch(path, data) {
        return fetch(this.baseUrl + path, {
            method: "PATCH",
            body: JSON.stringify(data),
            headers: this.headers,
            credentials: "same-origin"
        })
            .then(res => res.json())
            .then(res => this.transformer(res))
            .catch(reason => {
                return Promise.resolve(reason)
            })
    }

    put(path, data) {
        return fetch(this.baseUrl + path, {
            method: "PUT",
            headers: this.headers,
            body: JSON.stringify(data),
            credentials: "same-origin"
        })
            .then(res => res.json())
            .then(res => this.transformer(res))
            .catch(reason => {
                return Promise.resolve(reason)
            })
    }

    delete(path) {
        return fetch(this.baseUrl + path, {
            method: "DELETE",
            headers: this.headers,
            credentials: "same-origin"
        })
            .then(res => res.json())
            .then(res => this.transformer(res))
            .catch(reason => {
                return Promise.resolve(reason)
            })
    }

}

export default ApiClient