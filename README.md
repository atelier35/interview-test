# TV Show app

The TV Show app is a simple web app to search tv shows across open APIs available.
So far, it integrates with [TV MAZE](http://www.tvmaze.com/api#show-search) but we want more!

We used React to build this single page app that is connected to an API made in Go.


## Tasks

 1. First, create a branch with your name in it and start coding from there. 
 2. We want to allow the user to search from [The Movie DB API](https://developers.themoviedb.org/3/search/search-tv-shows). To do so, you'll have to add a new integration in the API by following the same structure than the TV MAZE integration.
	* You'll need an API key to call this api : `b081bf244cd3b93471785b06ab7da479` (use the v3 API to simplify the authentication process)
	* You can find the source of the API in `/go/src/bitbucket.org/atelier35/interview-test/`
 3. We want to add a new feature that will allow user to search for actors and actresses. In a similar way than the tv show search, we want to show when possible: the name, the picture and the age. We want to support the people search from both TV MAZE and The Movie DB. Describe what's need to be done to support that new feature and how you would implement it.

## Prerequisite

To run and test the code you will need to install:

 - Docker for [mac](https://docs.docker.com/v17.12/docker-for-mac/install/) or [windows](https://docs.docker.com/v17.12/docker-for-windows/install/)
 - [Yarn](https://yarnpkg.com/lang/en/docs/install) 
 - [Go](https://golang.org/doc/install)
 - [Go dep](https://golang.github.io/dep/docs/installation.html)

## Build

From the root path run:
```make build```

The API (in go) is compiled directly on the docker container. If you want to compile in local as well, you'll have to:

  - Set your go path to the go folder `./go`
  - from the folder `./go/src/bitbucket.org/atelier35/interview-api` you need to run `dep ensure` to install dependancies
  - You can build the app running `go install`
 
 We recommand to use [JetBrains GoLand IDE](https://www.jetbrains.com/go/) for the API and [JetBrains Webstorm IDE](https://www.jetbrains.com/webstorm/) for the UI

## Run

In two different terminal run:

```make up``` (this will start both containers)

and

```make watch``` (this will compile the javascript in real-time)

You can then access the app from: [http://localhost:3001](http://localhost:3001)




