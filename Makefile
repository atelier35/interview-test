.PHONY: help

# Add the following 'help' target to your Makefile
# And add help text after each target name starting with '\#\#'
# A category can be added with @category


help: ## This help dialog.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}';

up: ## Start environment
	docker-compose up;

down: ## Shutdown environment
	docker-compose down;

build: ## Build environment
	cd ./ui && yarn install;
	docker-compose build;

restart: down up ## Re-start environment

logs: ## Display and follow logs from the environment
	docker-compose logs -f;

build-api-and-start: ## Build the api container and start the app
	docker-compose build api;
	docker-compose up; 

watch: 
	cd ./ui && yarn watch;

